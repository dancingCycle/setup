# Sponge Setup

* update Debian's package index
```
sudo apt update
```

*  install Sponge package from Debian's APT repository
```
sudo apt install moreutils --no-install-recommends
```

* check the Sponge version
```
sponge -h
```
