# Setup Sphinx documentation generator

* get the latest version of installed packages and refresh the repo cache
```
sudo apt update
```

* install Sphinx directly from the official repository
```
sudo apt-get install python3-sphinx --no-install-recommends
