# Postgis Setup

* update Debian's package index
```
sudo apt update
```

*  install Postgis package from Debian's APT repository
```
sudo apt install postgis postgresql-15-postgis-3 --no-install-recommends
```
