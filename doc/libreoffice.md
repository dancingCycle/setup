# Libreoffice Setup

[source](https://installati.one/install-libreoffice-debian-12/)

* update Debian's package index
```
sudo apt update && sudo apt upgrade
```

*  install Libreoffice package from Debian's APT repository
```
sudo apt install libreoffice --no-install-recommends
```

* check the Libreoffice version
```
libreoffice --version
```
