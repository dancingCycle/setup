# Setup osmconvert

* update Apt repository cache
```
sudo apt update
```

* install
```
sudo apt install osm2pgsql --no-install-recommends
```
