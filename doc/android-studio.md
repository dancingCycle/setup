* download from [web](https://developer.android.com/studio#downloads)

* extract
```
tar -xvzf android-studio-2022.3.1.19-linux.tar.gz 
```

* execute
```
android-studio/bin/studio.sh &
```

* configure VM acceleration on Linux
```
sudo apt-get install cpu-checker --no-install-recommends
egrep -c '(vmx|svm)' /proc/cpuinfo
lsmod | grep kvm
whereis kvm-ok
sudo kvm-ok
```
