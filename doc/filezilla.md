# Setup Filezilla

* update apt repository cache
```
sudo apt update
```

## Using APT

* install Filezilla
```
sudo apt-get install filezilla --no-install-recommends
```

* verify Filezilla version
```
filezilla --version
```