# Zip Setup

* update Debian's package index
```
sudo apt update
```

*  install Zip package from Debian's APT repository
```
sudo apt install unzip zip --no-install-recommends
```

* check the Zip version
```
unzip -v
zip -v
```
