# Setup Docker Compose Plugin

## General

* update your APT cache using
```
sudo apt update
```

## Prerequisites

* install Docker like [this](./docker.md)

## Install Docker Compose Plugin

* install
```
sudo apt-get install docker-compose-plugin
```

* verify installation
```
docker compose version
```

## Links

[Install Docker Compose CLI plugin](https://docs.docker.com/compose/install/compose-plugin/#installing-compose-on-linux-systems)
