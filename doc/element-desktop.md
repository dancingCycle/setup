# Element Setup

[source](https://vegastack.com/tutorials/how-to-install-element-desktop-on-debian-12/)

* update Debian's package index
```
sudo apt update && sudo apt upgrade
```

* install the Initial Required Packages For Element Installation
```
sudo apt install software-properties-common apt-transport-https curl --no-install-recommends
```

* download Element Desktop APT Repository
```
curl -fSsL https://packages.element.io/debian/element-io-archive-keyring.gpg | gpg --dearmor | sudo tee /usr/share/keyrings/element-io-archive-keyring.gpg > /dev/null
```

* import the APT repository
```
echo "deb [signed-by=/usr/share/keyrings/element-io-archive-keyring.gpg] https://packages.element.io/debian/ default main" | sudo tee /etc/apt/sources.list.d/element-io.list
```

* update to reflect the new additional repository
```
sudo apt update
```

*  install Element package from Debian's APT repository
```
sudo apt install element-desktop --no-install-recommends
```

* check the Element version
```
element-desktop --version
```

# Cleanup

* first, remove the Element Desktop application using the following command
```
sudo apt remove element-desktop
```

* remove the repository using the following command
```
sudo rm /etc/apt/sources.list.d/element-io.list
```

* remove the GPG key as follows
```
sudo rm /usr/share/keyrings/element-io-archive-keyring.gpg
```
