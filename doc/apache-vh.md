# Source

[setting up VH](https://www.digitalocean.com/community/tutorials/how-to-install-the-apache-web-server-on-debian-11#step-5-setting-up-virtual-hosts-recommended)

# Setup

* create directory for our domain
```
sudo mkdir -p /var/www/vm2037.swingbe.mooo.com
```

* assign ownership of the directory to the user you’re currently signed in as with the `$USER` environment variable
```
sudo chown -R $USER:$USER /var/www/vm2037.swingbe.mooo.com
```

* ensure that your permissions are correct and allow the owner to read, write, and execute the files while granting only read and execute permissions to groups and others, you can input the following command
```
sudo chmod -R 755 /var/www/vm2037.swingbe.mooo.com
```

* create a sample index.html page with this instruction
```
vi /var/www/vm2037.swingbe.mooo.com/index.html
```

* and enter this content
```
<html>
    <head>
        <title>Hello World!</title>
    </head>
    <body>
        <h1>Success! The virtual host is working!</h1>
    </body>
</html>
```

* create a new virtual host file with the correct directives
```
sudo vi /etc/apache2/sites-available/vm2037.swingbe.mooo.com.conf
```

* and enter this content
```
<VirtualHost *:80>
    ServerAdmin admin@swingbe.de
    ServerName vm2037.swingbe.mooo.com
    ServerAlias www.vm2037.swingbe.mooo.com
    DocumentRoot /var/www/vm2037.swingbe.mooo.com
    ErrorLog ${APACHE_LOG_DIR}/vm2037.swingbe.mooo.com.error.log
    CustomLog ${APACHE_LOG_DIR}/vm2037.swingbe.mooo.com.access.log combined
</VirtualHost>
```

* enable the file with the a2ensite tool
```
sudo a2ensite vm2037.swingbe.mooo.com.conf
```

* disable the default site
```
sudo a2dissite 000-default.conf
```

* test for configuration errors
```
sudo apache2ctl configtest
```

* restart Apache to implement your changes
```
sudo systemctl restart apache2
```