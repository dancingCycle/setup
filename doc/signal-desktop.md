# Signal Setup

[source](https://www.linuxcapable.com/how-to-install-signal-on-debian-linux/)

* update Debian's package index
```
sudo apt update && sudo apt upgrade
```

* install the Initial Required Packages For Signal Installation
```
sudo apt install dirmngr ca-certificates software-properties-common apt-transport-https curl --no-install-recommends
```

* download Signal Desktop APT Repository
```
curl -fsSL https://updates.signal.org/desktop/apt/keys.asc | gpg --dearmor | sudo tee /usr/share/keyrings/signal-desktop-keyring.gpg > /dev/null
//ALTERNATIVE:wget -O- https://updates.signal.org/desktop/apt/keys.asc | gpg --dearmor > signal-desktop-keyring.gpg
//ALTERNATIVE:cat signal-desktop-keyring.gpg | sudo tee /usr/share/keyrings/signal-desktop-keyring.gpg > /dev/null
```

* import the APT repository
```
echo "deb [arch=amd64 signed-by=/usr/share/keyrings/signal-desktop-keyring.gpg] https://updates.signal.org/desktop/apt xenial main" | sudo tee /etc/apt/sources.list.d/signal-desktop.list
```

* update to reflect the new additional repository
```
sudo apt update
```

*  install Signal package from Debian's APT repository
```
sudo apt install signal-desktop --no-install-recommends
```

* check the Signal version
```
signal-desktop --version
```

# Cleanup

* first, remove the Signal Desktop application using the following command
```
sudo apt remove signal-desktop
```

* remove the repository using the following command
```
sudo rm /etc/apt/sources.list.d/signal-desktop.list
```

* remove the GPG key as follows
```
sudo rm /usr/share/keyrings/signal-desktop-keyring.gpg
```