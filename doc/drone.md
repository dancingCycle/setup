# Setup Drone

## General

* update your APT cache using
```
sudo apt update
```

* upgrade packages
```
sudo apt upgrade
```

* setup Docker like [this](./docker.md)

* download and install Drone
```
curl -L https://github.com/harness/drone-cli/releases/latest/download/drone_linux_amd64.tar.gz | tar zx
sudo install -t /usr/local/bin drone
```

## Configuration

sudo mkdir -p /etc/drone/
sudo vi /etc/drone/drone.toml

* insert port configuration like this
```
[server]
port=":80"
```

sudo vi /etc/drone/drone.toml

* insert GitHub details like this
```
[github]
client="<enter Client ID from GitHub for this app>"
secret="<enter the Client Secret from GitHub for this app>"
# orgs=[]
# open=false
```

## Drone Server

* download the server like this
```
docker pull drone/drone:latest
```

* verify that the latest server image is downloaded like this
```
docker image ls
```

* start the server like this
```
docker run \
  --volume=/var/lib/drone:/data \
  --env=DRONE_GITHUB_CLIENT_ID=your-id \
  --env=DRONE_GITHUB_CLIENT_SECRET=super-duper-secret \
  --env=DRONE_RPC_SECRET=super-duper-secret \
  --env=DRONE_SERVER_HOST=drone.company.com \
  --env=DRONE_SERVER_PROTO=https \
  --publish=80:80 \
  --publish=443:443 \
  --restart=always \
  --detach=true \
  --name=drone \
  drone/drone:latest
```

* check logs
```
docker logs <container name<
```

## Runners

### Set up Docker Runner

* pull public image like this
```
docker pull drone/drone-runner-docker:latest
```

* start the runner like this
```
docker run --detach \
  --volume=/var/run/docker.sock:/var/run/docker.sock \
  --env=DRONE_RPC_PROTO=https \
  --env=DRONE_RPC_HOST=drone.company.com \
  --env=DRONE_RPC_SECRET=super-duper-secret \
  --env=DRONE_RUNNER_CAPACITY=2 \
  --env=DRONE_RUNNER_NAME=my-first-runner \
  --publish=3000:3000 \
  --restart=always \
  --name=runner \
  drone/drone-runner-docker:latest
```

* check logs
```
docker logs <container name<
```

## Links

https://docs.drone.io/cli/install/

https://foxutech.com/how-to-setup-drone/

https://webhookrelay.com/blog/2019/02/11/using-drone-for-simple-selfhosted-ci-cd/

https://laptrinhx.com/how-to-setup-drone-a-continuous-integration-service-in-linux-3923450899/

https://docs.drone.io/server/provider/github/
