# Disable Start Up At System Boot

* If you install the PostgreSQL database from packages, it is automatically added to the start up scripts of the operating system. If you are only learning to work with the database, it is unnecessary to start the database each time you boot the system. Remove system startup link for the PostgreSQL database
```
sudo update-rc.d -f postgresql remove
```

