# Create Database with Postgres

* create a new database `database name` and grant privileges to `user name`
```
CREATE DATABASE <database name> OWNER <user name>;
```
