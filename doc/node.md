# Setup Node

* update apt repository cache
```
sudo apt update
```

## Using APT

* install node.js and npm
```
sudo apt-get install nodejs npm --no-install-recommends
```

## Using NVM

* configure Node.js: [Here](https://github.com/nvm-sh/nvm) we find a bash script to manage multiple active node.js versions
```
node -v
which node
chmod 700 ~/.profile
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.7/install.sh | bash
command -v nvm
nvm -v
. ~/.profile
nvm -v
command -v nvm
echo $NVM_DIR
```

* OPTION: log out and log in again so that the configuration is updated

* Select and Install a Node.js version
```
nvm version-remote --lts
nvm ls-remote| grep 'Latest LTS'| grep v18
nvm i <lts>
```

* NOTE: Make sure node.js is available in the correct version
```
which node
```

* OPTION: Compare nvm's and user's Node.js version
```
/usr/bin/node -v
node -v
```

### Helpful NVM instructions

* install specific node version
```
nvm install 12.20.1
```

* list installed node versions for the current user
```
nvm ls
```

* find available node versions for installation
```
nvm ls-remote
```

* select a different node version for the current session only
```
nvm use 12.20.1
```

* find the default version for the current user
```
nvm run default --version
```

* run node script with the desired node version
```
nvm exec 12.20.1 <node script>.js
```
