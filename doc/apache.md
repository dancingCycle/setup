# Setup Apache

* get the latest version of installed packages and refresh the repo cache
```
sudo apt update
```

* install directly from the official repository
```
sudo apt-get install apache2 --no-install-recommends
```

* verify Apache version
```
sudo apache2 -version
```

* check if Apache is running without any issues
```
sudo systemctl status apache2
```

* UFW users can open HTTP (80) and HTTPS (443) ports like this
```
sudo ufw status
sudo ufw allow 80/tcp
sudo ufw allow 443/tcp
sudo ufw status
```

* verify Apache is working in a browser
```
hostname -I
http://<server ip>
```

* OPTION: enable web service proxy modules like [this](./apache-proxy.md)

* OPTION: enable SSL like [this](./apache-ssl.md)

* OPTION: set up virtual host [this](./apache-vh.md)
