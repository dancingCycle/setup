# Setup Postgres

* install Postgres like [this](./postgres-install.md)

* create copy of config file ```pg_hba.conf file```\
NOTE: Insert the postgresql version <psql version> that is running on the respective host
```
cd /etc/postgresql/<psql version>/main/
sudo cp pg_hba.conf pg_hba.conf-bckp
```

* switch authentication method from ```peer``` to ```md5``` for all local users connecting by Unix domain sockets by opening the following config file and adding the example configuration
```
sudo vi pg_hba.conf
```

```
# "local" is for Unix domain socket connections only
#local   all             all                                     peer
local   all             all                                     md5
```

* restart PostgreSQL to enable the changes
```
systemctl status postgresql
sudo systemctl restart postgresql
systemctl status postgresql
```

* OPTION: allow remote connections like [this](./postgres-connect-remote.md)

* OPTION: disable start up at system boot like [this](./postgres-boot-startup-disable.md)
