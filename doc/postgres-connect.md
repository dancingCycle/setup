# Connect to Postgres Database Using Psql

* OPTION: connect to Postgres as admin user
```
sudo -u postgres psql -U postgres
```

* OPTION: connect to the database `database name` as user `user name`
```
psql -U <user name> -d <database name>

```

* OPTION: connect to the database `database name` as user `user name` to a remote Postgres instance at host `host name` and port `port number` (NOTE: Configure Postgres for remote connections like [this](./postgres.md))
```
psql -h <host name> -p <port number> -U <user name> -d <database name>
```