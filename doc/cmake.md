* update cache
```
sudo apt update
```

* install
```
sudo apt install cmake --no-install-recommends
```

* confirm installed version
```
cmake --version
```

* to make use of cmake we might want to install the package ```build-essential```
```
sudo apt install build-essential --no-install-recommends
gcc --version
```
