# Thunderbird Setup

[source](https://installati.one/install-thunderbird-debian-12/)

* update Debian's package index
```
sudo apt update && sudo apt upgrade
```

*  install Thunderbird package from Debian's APT repository
```
sudo apt install thunderbird --no-install-recommends
```

* check the Thunderbird version
```
thunderbird --version
```
