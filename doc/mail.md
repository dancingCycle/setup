# Setup mail

* update Apt repository cache
```
sudo apt update
```

* install
```
sudo apt install mailutils msmtp --no-install-recommends
```

* study
```
https://unix.stackexchange.com/questions/640296/configuring-mail-command-to-use-msmtp
https://moritzvd.com/email-with-smtp-debian-ubuntu/
https://caupo.ee/blog/2020/07/05/how-to-install-msmtp-to-debian-10-for-sending-emails-with-gmail/
https://phoenixnap.com/kb/linux-mail-command
```

* foo
NOTE: read [this](https://superuser.com/questions/351841/how-do-i-set-up-the-unix-mail-command)
```
sudo apt install mailutils msmtp --no-install-recommends
emacs ~/.msmtprc
emacs ~/.mailrc
emacs ~/.netrc
chmod 600 ~/.netrc 
```

* foo
```
sudo apt install mailutils --no-install-recommends
sudo apt install mailutils msmtp --no-install-recommends
emacs ~/.msmtprc
sudo apt-get install msmtp msmtp-mta
sudo vi /etc/msmtprc
sudo apt-get install bsd-mailx
sudo vi /etc/mail.rc
 ```

* test
```
echo "Message" | mail -s "Title" sbegerad@posteo.de
echo "Message" | mail -s "Title" dialog@swingbe.de
 ```