# Enable ssh-agent

* check if ssh-agent is running
```
env | grep -i ssh
```

* start the ssh-agent in the background
```
eval "$(ssh-agent -s)"
```

* verify if ssh-agent is running now
```
env | grep -i ssh
```

* list keys managed by the agent
```
ssh-add -l
```

* add your ssh key
```
ssh-add ~/.ssh/<e.g. id_ed25519 file>
```

* verify keys managed by the agent
```
ssh-add -l
```
