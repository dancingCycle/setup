# Setup Psql Client

* get the latest version of installed packages and refresh the repo cache
```
sudo apt update
```

* install postgres directly from the official repository
```
sudo apt-get install postgresql-client --no-install-recommends
```
