According to e.g. this [guide](https://www.heise.de/tipps-tricks/Mit-DDNS-auf-s-Heimnetzwerk-zugreifen-4319924.html) you can access your host in the Internet via IP like this.

* login at [FreeDNS](https://freedns.afraid.org/)

* Wechseln Sie zum Bereich "Subdomains" und legen Sie über den "Add"-Button eine Verknüpfung an.

* Wählen Sie als "Subdomain" einen beliebigen Namen und bei "Domain" einen beliebigen Eintrag aus der Liste. Daraus ergibt sich dann die Webadresse in der Form "Subdomain.Domain". Bei "Destination" die entsprechende IP-Adresse eintragen. Kontrollieren können Sie das im Zweifel einfach über die Seite [Ifconfig.me](https://ifconfig.me/).