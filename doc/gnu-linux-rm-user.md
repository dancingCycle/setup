* list active users
```
who
```

* list processes running by <user>
```
ps -u <user>
top -U <user>
```

* look at the <user> entry of the password list
```
sudo awk -F: '/<user>/ {print $1,$2}' /etc/shadow
```

* lock the account
```
sudo passwd -l <user>
```

* validate lock
```
sudo awk -F: '/<user>/ {print $1,$2}' /etc/shadow
```

* kill <user> processes
```
sudo pkill -KILL -u <user>
```

* validate <user> process list
```
who
```

* check scheduled cron jobs for <user>
```
sudo ls -lh /var/spool/cron/crontabs/<user>
```

* delete cron jobs of <user>
```
sudo crontab -r -u <user>
```

* remove print jobs of <user>
```
lprm -U <user>
```

* remove user
```
sudo deluser --remove-home <user>
```

* check that home dir is removed
```
ls /home
```

* check group
```
less /etc/group | grep <user>
```