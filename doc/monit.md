# Update The System

```
sudo apt-get update
sudo apt-get upgrade
```

Once your system is updated, restart your system to make these changes take effect with the following command:

```
exit
login
```

# Installing Monit

```
sudo apt install monit --no-install-recommends
```

Once Monit is installed, start Monit service and enable it to start on boot time by running the following command:

```
sudo systemctl start monit
sudo systemctl enable monit
```

You can check the status of Monit service any time by running the following command:

```
systemctl status monit
```

Backup config and enable integrated http daemon

```
sudo cp /etc/monit/monitrc /etc/monit/monitrc-orgnl
sudo vi /etc/monit/monitrc-orgnl
sudo systemctl restart monit
systemctl status monit
```

You can see the status of Monit with the following command:

```
sudo monit status
```

To check configuration files syntax, use:

```
sudo monit -t
```

Check monitored processes summary:

```
sudo monit  summary
```

Configure Monit for Apache Service

By default Monit comes with the pre-defined template for some of process and services. You can find these templates in /etc/monit/conf-available/ directory. By default template for Apache is available in Monit. So you will only need to enable it. You can enable it by running the following command:

```
sudo ln -s /etc/monit/conf-available/apache2 /etc/monit/conf-enabled/
```

Next, restart monit servcie to apply these changes with the following command:

```
sudo systemctl restart monit
systemctl status monit
```

To start running all of the monitored programs.

```
sudo monit start all
```
