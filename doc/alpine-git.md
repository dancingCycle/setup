# Setup Git

* update your APT cache using
```
doas apk update
```

* install Git from Debian's APT repository
```
doas apk add git
```

* verify installation
```
git --version
```

* provide contact details to be embedded in the commit message
```
git config --global user.name "<tbd>"
git config --global user.email "<tbd>"
```

* verify contact details
```
git config --list
```
