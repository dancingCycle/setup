# Allow Remote Connections

* configure ports and update firewall using the [firewall setup](firewall.md)
```
sudo ufw allow 5432
//OPTION: sudo ufw allow 5432
sudo ufw enable
sudo ufw status numbered
```

* create config backup
```
sudo cp /etc/postgresql/<version>/main/postgresql.conf /etc/postgresql/<version>/main/postgresql.conf-backup
```

* open config file to define what IP addresses postgres to listen on
```
sudo vi /etc/postgresql/<version>/main/postgresql.conf
```

* edit config file like this
```
#listen_addresses = 'localhost'
listen_addresses = '*'
```

* open config file to define access to all databases for all users with an encrypted key
```
sudo vi /etc/postgresql/<version>/main/pg_hba.conf
```

* edit config file like this
```
# TYPE  DATABASE	USER	ADDRESS   	METHOD
host    all     	all     0.0.0.0/0       md5
host    all             all     :/0             md5
```

* restart PostgreSQL to enable the changes
```
sudo systemctl restart postgresql
systemctl status postgresql
```
