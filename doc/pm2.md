# Setup pm2

* configure pm2 like [this](https://pm2.io/docs/runtime/guide/installation/) to start API on system startup

//OPTION: * with npm
```
npm install pm2 -g
```

//OPTION: * with GNU/Debian
```
sudo apt update && sudo apt install sudo curl && curl -sL https://raw.githubusercontent.com/Unitech/pm2/master/packager/setup.deb.sh | sudo -E bash -
```

* logout and login in again to make the instructions above available
```
exit
ssh <user>@<host>
```

* verify installation of pm2
```
pm2 --version
```

* initialize pm2 as a startup service
```
pm2 startup
//ON-DEMAND: sudo env PATH=$PATH:/home/api-data.vbn.de/.nvm/versions/node/v18.17.1/bin /home/api-data.vbn.de/.nvm/versions/node/v18.17.1/lib/node_modules/pm2/bin/pm2 startup systemd -u api-data.vbn.de --hp <enter path>
```

# Options

* update pm2 with npm
```
npm install pm2 -g && pm2 update
```

* install pm2 autocompletion
```
pm2 completion install
```

* restart/logout/login your system, and check if all the serviceis running under PM2
```
pm2 ls
```
or
```
pm2 status
```

* monitor logs, custom metrics, process information from all processes
```
pm2 monit
```

* view details of a single Node process using the process ID or name
```
pm2 show <id>
pm2 show <name>
```

* list of some common process (single or all) management command
```
$ sudo pm2 stop all                  		#stop all apps
$ sudo pm2 stop 0                    		#stop process with ID 0
$ sudo pm2 restart all               		#restart all apps
$ sudo pm2 reset 0		         	#reset all counters
$ sudo pm2 delete all                		#kill and remove all apps
$ sudo pm2 delete 1                 		#kill and delete app with ID 1
```

* manage application logs
```
$ sudo pm2 logs                      	#view logs for all processes 
$ sudo pm2 logs 1	         	#view logs for app 1
$ sudo pm2 logs --json               	#view logs for all processes in JSON format
$ sudo pm2 flush			#flush all logs
```

* manage the PM2 process
```
$ sudo pm2 startup            #enable PM2 to start at system boot
$ sudo pm2 startup systemd    #or explicitly specify systemd as startup system 
$ sudo pm2 save               #save current process list on reboot
$ sudo pm2 unstartup          #disable PM2 from starting at system boot
$ sudo pm2 update	      #update PM2 package
```
