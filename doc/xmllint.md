# Setup xmllint

* update your APT cache using
```
sudo apt update
```

* install
```
sudo apt install libxml2-utils --no-install-recommends
```

* verify the installation by checking the APT-CACHE Policy using
```
sudo apt-cache policy libxml2-utils
```
