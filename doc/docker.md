# Setup Docker on Debian (June 2024)

## General

[source](https://docs.docker.com/engine/install/debian/

* update your APT cache using
```
sudo apt update
```

* uninstall old versions
```
sudo apt-get remove docker docker-engine docker.io containerd runc docker-compose docker-doc podman-docker
```

* install basic package dependencies
```
sudo apt install apt-transport-https ca-certificates curl gnupg lsb-release --no-install-recommends
```

* uninstall all conflicting packages
```
for pkg in docker.io docker-doc docker-compose podman-docker containerd runc; do sudo apt-get remove $pkg; done
```

* clean up any existing data
```
sudo apt-get purge docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin docker-ce-rootless-extras
sudo rm -rf /var/lib/docker
sudo rm -rf /var/lib/containerd
```

* set up Docker's apt repository
```
# Add Docker's official GPG key:
sudo apt-get update
sudo apt-get install ca-certificates curl
sudo install -m 0755 -d /etc/apt/keyrings
sudo curl -fsSL https://download.docker.com/linux/debian/gpg -o /etc/apt/keyrings/docker.asc
sudo chmod a+r /etc/apt/keyrings/docker.asc

# Add the repository to Apt sources:
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.asc] https://download.docker.com/linux/debian \
  $(. /etc/os-release && echo "$VERSION_CODENAME") stable" | \
  sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt-get update
```

* install the Docker packages
```
sudo apt-get install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin
```

* verify that the installation is successful by running the hello-world image
```
sudo docker run hello-world
```

OPTION: check whether the service is enabled
```
systemctl is-enabled docker
systemctl is-enabled containerd
```

OPTION: check Docker and Containerd service status
```
systemctl status docker containerd
```

OPTION: verify Docker version
```
docker -v
```

OPTION: add the user <user> to the group ‘docker‘
```
sudo usermod -aG docker <user>
```

OPTION: log in as user <user> using the command below and verify the configuration
```
su - <user>
```

OPTION: run the following docker command to verify your installation
```
docker run hello-world
```
