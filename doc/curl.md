# Curl Setup

* update Debian's package index
```
sudo apt update
```

*  install Curl package from Debian's APT repository
```
sudo apt install curl --no-install-recommends
```

* check the Curl version
```
curl --version
```
