* check web service port configuration of `ssl_module` for port 443
```
sudo a2enmod ssl
sudo systemctl restart apache2
systemctl status apache2
sudo apachectl configtest
less /etc/apache2/ports.conf
```

* OPTION: check firewall (nftables) configuration for port 443
```
less /etc/nftables.conf
```

* OPTION: check firewall (ufw) configuration for port 443
```
sudo ufw status
```

* test API in browser
```
https://<hostname -I>
```