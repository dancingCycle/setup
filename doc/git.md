# Git Setup

* update Debian's package index
```
sudo apt update
```

*  install Git package from Debian's APT repository
```
sudo apt install git --no-install-recommends
```

* check the Git version
```
git --version
```
