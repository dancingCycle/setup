#  Installation/FromUSBStick

[link](https://help.ubuntu.com/community/Installation/FromUSBStick)

# Create Bootable USB Drive Using mkusbmin

[link](https://help.ubuntu.com/community/mkusb/min)

# Create Bootable USB Drive Using DebianInstall

[link](https://wiki.debian.org/DebianInstall)

# Create Bootable USB Drive Using UnetBootin

[link](https://unetbootin.github.io/linux_download.html)

# Installation + Archive + USBStick

[link](https://wiki.debian.org/Installation+Archive+USBStick#Preparing_the_USB_Stick)

# CreateUSBMedia

[link](https://wiki.debian.org/DebianInstaller/CreateUSBMedia)

# Create Bootable USB Drive Using Balena Etcher

[link](https://www.pragmaticlinux.com/2021/03/create-a-bootable-usb-drive-from-a-linux-iso-image/)

# Create Bootable USB Drive Manually

* download debian

* mount USB drive

* identify USB drive using
```
df -h
```

* unmount drive using
```
sudo umount /dev/<tbd>
```

* format drive using
```
sudo mkfs.vfat /dev/<tbd>
```

* identify the device name
```
lsblk -p | grep "disk"
```

* make sure the USB drive is not mounted
```
sudo umount /dev/<tdb>
```

* write the ISO file to the USB drive using the following options
```
sudo dd if=[ISO FILE] of=[DEVICE NAME] bs=4M conv=fdatasync status=progress
sudo dd if=[ISO FILE] of=[DEVICE NAME] bs=4M status=progress oflag=sync
sudo dd if=[ISO FILE] of=[DEVICE NAME] bs=4M status=progress oflag=sync
```