# Setup osmconvert

* update Apt repository cache
```
sudo apt update
```

* install
```
sudo apt install osmctools --no-install-recommends
```
