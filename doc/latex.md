# Setup Latex

* get the latest version of installed packages and refresh the repo cache

```
sudo apt update
```

* install directly from the official repository

```
sudo apt install texlive-base --no-install-recommends
sudo apt-get install texlive-latex-base --no-install-recommends
```

* verify installation

```
latex --version
pdflatex --version
```
