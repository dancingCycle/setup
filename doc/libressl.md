# install libressl

[download link](https://ftp.openbsd.org/pub/OpenBSD/LibreSSL/)

* download latest tarball from download link
```
cd ~
mkdir tmp && cd tmp
wget https://ftp.openbsd.org/pub/OpenBSD/LibreSSL/libressl-3.7.0.tar.gz
tar -xzvf libressl
cd libressl
mkdir build && cd build
../configure
make
sudo make install
sudo ldconfig
```
