# Create User

* Does the user "usr" already exist?
```
sudo grep "usr" /etc/shadow
```

* create user "usr" with default home folder
```
sudo useradd -m -s /usr/bin/bash "usr"
```
* create password
```
sudo passwd "usr"
```
* add user "usr" to sudo group
```
sudo usermod -a -G sudo "usr"
```
