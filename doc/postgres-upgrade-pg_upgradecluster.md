# Upgrade using pg_upgradecluster

* check clusters
```
pg_lsclusters
```

* do
```
sudo apt update
sudo apt upgrade postgresql-xy
```

* install relevant packages
```
sudo apt-get install postgresql-server-dev-xy
```

* make a backup
```
cd ~
time sudo -u postgres pg_dumpall > ~/pg_dumpall-bckp-all-2023-08-07
```

* check installed extensions on old version
```
sudo -u postgres psql -p 5432 postgres
\dx
```

* OPTION: install extension on new version
```
sudo apt install postgresql-xy-postgis-3 --no-install-recommends
//OPTION: sudo -u postgres psql -p 5432 postgres
//OPTION: sudo -u postgres psql -p 5433 postgres
\dx
CREATE EXTENSION postgis;
CREATE EXTENSION hstore;
\q
```

* stop all clusters
```
sudo systemctl stop postgresql
systemctl status postgresql
pg_lsclusters
```

* migrate
```
sudo systemctl stop postgresql@13-main
sudo -u postgres pg_dropcluster 13 main
sudo -u postgres pg_upgradecluster 11 main
sudo systemctl daemon-reload
systemctl status postgresql@13-main
pg_lsclusters
```

* drop the old cluster:
```
sudo -u postgres pg_dropcluster 11 main
```

* uninstall the previous version
```
sudo apt purge postgresql-11* postgresql-client-11*
sudo apt autoremove
apt list --installed | grep postgresql-11 postgresql-client-11*
```

* do
```
sudo pg_ctlcluster 13 main stop
pg_lsclusters
sudo pg_ctlcluster 13 main start
pg_lsclusters
```

* vacuum
```
sudo -u postgres vacuumdb --all --analyze-only
sudo -u postgres vacuumdb --all
```
