# Upgrade using pg_upgrade

* check clusters
```
pg_lsclusters
```

* do
```
sudo apt update
sudo apt upgrade postgresql-xy
```

* install relevant packages
```
sudo apt-get install postgresql-server-dev-13
```

* make a backup
```
cd
time sudo -u postgres pg_dumpall > bckp-all-2023-08-07
```

* check installed extensions on old version
```
sudo -u postgres psql -p 5432 postgres
\dx
```

* install extension on new version
```
sudo apt install postgresql-13-postgis-3 --no-install-recommends
sudo -u postgres psql -p 5433 postgres
\dx
CREATE EXTENSION postgis;
CREATE EXTENSION hstore;
```

* stop all clusters
```
sudo systemctl stop postgresql
systemctl status postgresql
pg_lsclusters
```

* migrate
```
sudo -iu postgres /usr/lib/postgresql/13/bin/pg_upgrade \
-o "-c config_file=/etc/postgresql/11/main/postgresql.conf" \
--old-datadir=/var/lib/postgresql/11/main/ \
-O "-c config_file=/etc/postgresql/13/main/postgresql.conf" \
--new-datadir=/var/lib/postgresql/13/main/ \
--old-bindir=/usr/lib/postgresql/11/bin \
--new-bindir=/usr/lib/postgresql/13/bin --check --jobs=4
```

* TODO tbc

* uninstall the previous version
```
sudo apt purge postgresql-11* postgresql-client- 11*
sudo apt autoremove
apt list --installed | grep postgresql-11 postgresql-client-11*
```
