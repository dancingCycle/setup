# Source

[How To Secure Apache with Let's Encrypt on Debian 11](https://www.digitalocean.com/community/tutorials/how-to-secure-apache-with-let-s-encrypt-on-debian-11)

[Use Certbot to Enable HTTPS with Apache on Debian](https://www.linode.com/docs/guides/enabling-https-using-certbot-with-apache-on-debian/)

[Certbot Instructions](https://certbot.eff.org/instructions?ws=apache&os=debianbuster)

# Setup Certbot

* remove certbot-auto and any Certbot OS packages
```
sudo apt remove certbot
```

* install Certbot
```
sudo apt install snapd --no-install-recommends
sudo snap install core
sudo snap refresh core
sudo snap install --classic certbot
```

* prepare the Certbot command
```
sudo ln -s /snap/bin/certbot /usr/bin/certbot
```

* OPTION: set up virtual host [this](./apache-vh.md)

* check, open the virtual host file for your domain
```
sudo vi /etc/apache2/sites-available/vm2037.swingbe.mooo.com.conf
```

* verify the syntax of your configuration edits
```
sudo apache2ctl configtest
```

* verify the current firewall setting by running
```
sudo ufw status
```

* allow HTTPS traffic, allow the “WWW Full” profile and delete the redundant “WWW” profile allowance
```
sudo ufw allow 'WWW Full'
sudo ufw delete allow 'WWW'
sudo ufw status
```

* Certbot provides a variety of ways to obtain SSL certificates through plugins. The Apache plugin will take care of reconfiguring Apache and reloading the configuration whenever necessary. To use this plugin, run the following
```
sudo certbot --apache -d vm2037.swingbe.mooo.com
```

* Let’s Encrypt certificates are only valid for ninety days. This is to encourage users to automate their certificate renewal process. The certbot package you installed takes care of this for you by adding a renew script to /etc/cron.d. This script runs twice a day and will automatically renew any certificate that’s within thirty days of expiration. To test the renewal process, you can do a dry run with certbot
```
sudo certbot renew --dry-run
```

* test manual renewal
```
sudo /usr/bin/certbot renew --apache --quiet
```

//TODO Enable `MAILTO`!
* extend `crontab -e` for auto renewal with this content
```
@daily /usr/bin/certbot renew --apache --quiet
```
