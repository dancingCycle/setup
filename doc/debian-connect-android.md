* install `jmtpfs` like this

```
sudo apt install jmtpfs --no-install-recommends
```

* create folder as mount point like this

```
sudo mkdir -p /media/fp3
sudo chown begerad:begerad /media/fp3/
sudo chmod 755 /media/fp3/
```

* created two aliases on .bashrc @home

```
emacs ~/.bashrc &
```

```
alias fp3="jmtpfs /media/fp3"
alias unfp3="fusermount -u /media/fp3"`
``

NOTE:
* 1rs alias mounts the Android once pluged on system via phone cable/usb
* 2nd alias safely removes phone from system before disconnecting usb cable.

NOTE:
* Make sure the phone is not locked when writing those two commands (or at least the mount ones).

**NOTE**:
Before calling the following convenience alias,
make sure the Android device is in 'File Transfer' mode.

* on console write `fp3` or `unfp3`
