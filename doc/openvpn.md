# Setup openvpn

* update Apt repository cache
```
sudo apt update
```

* install
```
sudo apt install openvpn --no-install-recommends
```
