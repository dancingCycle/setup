# Setup systemd unit

* it is prudent to make sure none of the existing systemd unit files have the name we want to give our new service.
```
systemctl list-units --type=service
```

* create our unit file.
```
sudo vi /etc/systemd/system/<service name>.service
```

* paste text into the editor accoring to the <service name> needs

* give the owner read and write permissions, and read permissions to the group. Others will have no permissions.
```
sudo chmod 640 /etc/systemd/system/<service name>.service
```

* check the syntax of our unit file for us, even if the service isn't running yet
```
systemctl status <service name>.service
```

* When you add a new unit file or edit an existing one, you must tell systemd to reload the unit file definitions.
```
sudo systemctl daemon-reload
```

* If you want a service to be launched at startup you must enable it:
```
sudo systemctl enable <service name>
```

* Enabling a service doesn't start it, it only sets it to be launched at boot time. To start the service now, you must use systemctl with the start option.
```
sudo systemctl start <service name>
```

* verify that our service is running correctly.
```
sudo systemctl status <service name>.service
```

